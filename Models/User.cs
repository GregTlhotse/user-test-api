using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Threading.Tasks;  
  
namespace user_api.Models  
{  
    public class User{
        public Guid Id { get; set; }
        public  string FirstName { get; set; }
         public  string LastName { get; set; }
         public  string Email { get; set; }
        
         public  byte[] Password { get; set; }
         public  string Status { get; set; }
          public  string Mobile { get; set; }



    }  
}