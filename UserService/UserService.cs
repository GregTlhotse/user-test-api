

using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;
using user_api.Interface;
using user_api.Models;
using user_api.ViewModels;
using WebApi.Helpers;

namespace user_api.UserService{

public class UserService : IUserUservice
{

    readonly DataContext _dbcontext;
    public UserService(DataContext dbcontext)
    {
        _dbcontext = dbcontext;

    }
    public User Add(UserViewModel person)
    {
        byte[] hashedPassword = EncryptPassword(person.Password);
            var personInfo = new User{
                FirstName = person.FirstName,
                LastName = person.LastName,
                Email = person.Email,
                Mobile = person.Mobile,
                Password = hashedPassword,
                Status = person.Status
            }; 
            _dbcontext.Add(personInfo);
            _dbcontext.SaveChanges();
            return personInfo;

    }
    byte[] EncryptPassword(string password)
    {
        byte[] data = System.Text.Encoding.ASCII.GetBytes(password);
        MD5 md5 = MD5.Create();
        byte[] hash = md5.ComputeHash(data);
        return hash;
    }

}
}