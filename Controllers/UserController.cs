using Microsoft.AspNetCore.Mvc;
using user_api.Interface;
using user_api.UserService;
using user_api.ViewModels;

namespace user_api.Controllers
{
    [Route("api/[controller]")]

    public class UserController : Controller
    {
        readonly IUserUservice _service;
        public UserController(IUserUservice service)
        {
            _service = service;

        }


        [HttpPost]
        public IActionResult Post([FromBody] UserViewModel person)
        {
             return Ok(_service.Add(person));

        }
    }



}